<?php

function email_html($from,$destino,$subject,$cuerpo,$con_attach = 0,$attach)
{
	$headers = "From: $from <$from>\n";
	$headers .= "Reply-To: noreply@prilogic.com\n";
	$headers .= "MIME-Version: 1.0\n";
	$headers .= "Content-Type: multipart/mixed; boundary=\"MIME_BOUNDRY\"\n";
	$headers .= "X-Sender: $from_k <$from>\n";
	$headers .= "X-Mailer: PHP4\n"; 
	$headers .= "X-Priority: 3\n"; 
	$headers .= "Return-Path: <$from>\n";
	$headers .= "This is a multi-part message in MIME format.\n";
	$message = "--MIME_BOUNDRY\n";
	$message .= "Content-Type: text/html; charset=\"iso-8859-1\"\n";
	//$message .= "Content-Transfer-Encoding: quoted-printable\n";
	$message .= "\n";
	$message .= $cuerpo;
	$message .= "\n";

		if($con_attach=="1")
		{
			$filename = basename($attach);
			$file_url = $attach;
			$fp = fopen($file_url,"r");
			$str = fread($fp, filesize($file_url));
			$str = chunk_split(base64_encode($str));
			$message .= "--MIME_BOUNDRY\n";
			$message .= "Content-Type: application/octet-stream; name=\"$filename\"\n";
			$message .= "Content-disposition: attachment\n";
			$message .= "Content-Transfer-Encoding: base64\n";
			$message .= "\n";
			$message .= "$str\n";
			$message .= "\n";
			$message .= "--MIME_BOUNDRY--\n";
		}

		mail($destino, $subject, $message, $headers);
}