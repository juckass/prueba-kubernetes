<?php
global $config;

$set = "";

$mensaje_destacado = "";
$tipo = ""; $_SESSION['tipo'] = "";
$tipo_mensaje = "";
$numero_pedido = "";
$estado_pedido = "";


$fecha_retiro = ""; $_SESSION['fecha_retiro'] = "";

$sucursal_id = ""; $_SESSION['sucursal_id'] = "";

$mensaje_direccion = "";
$direccion = ""; $_SESSION['direccion'] = "";
$numero = ""; $_SESSION['numero'] = "";
$comuna = "";
$comuna_id = "";  $_SESSION['comuna_id'] = "";
$comuna_texto = "";
$telefono = ""; $_SESSION['telefono'] = "";
$telefono_texto = "";
$email = ""; $_SESSION['email'] = "";
$email_texto = "";
$fecha_despacho = ""; $_SESSION['fecha_despacho'] = "";
$comentarios = ""; $_SESSION['comentarios'] = "";
$comentarios_texto = "";

if (isset($_REQUEST['tipo']))
{
    $tipo = $_REQUEST['tipo'];
    $_SESSION['tipo'] = $tipo;
    
    if ($tipo == "domicilio")
    {
        $tipo_mensaje = "Entrega en Domicilio";  

        if (isset($_REQUEST['direccion']))
        {
            $direccion = $_REQUEST['direccion'];
            $_SESSION['direccion'] = $direccion;
        }

        if (isset($_REQUEST['numero']))
        {
            $numero = $_REQUEST['numero'];
            $_SESSION['numero'] = $numero;
        }

        if (isset($_REQUEST['comuna']))
        {
            $comuna = $_REQUEST['comuna'];
            list($comuna_id,$comuna_texto) = explode("|",$comuna);
            $comuna_texto = ", ". $comuna_texto;
            $_SESSION['comuna_id'] = $comuna_id;

            $stmt = "SELECT	fechas FROM	sector_fecha JOIN comunas ON sector_fecha.sector_id = comunas.sector_id WHERE	comunas.comuna_id = " . $comuna_id . ";";
			$sth = execstmt($config['conn'],$stmt);
			$res = mysql_fetch_array($sth);
			$fecha_despacho = $res['fechas'];
			$_SESSION['fecha_despacho'] = $fecha_despacho;
			$mensaje_destacado = "Fecha entrega: " . $fecha_despacho;
        }

        if (isset($_REQUEST['telefono']) && $_REQUEST['telefono'] != "")
        {
            $telefono = $_REQUEST['telefono'];
            $_SESSION['telefono'] = $telefono;
            $telefono_texto = "</p><p>Teléfono: " . $telefono;
        }
        
        if (isset($_REQUEST['email']) && $_REQUEST['email'] != "")
        {
            $email = $_REQUEST['email'];
            $_SESSION['email'] = $email;
            $email_texto = "</p><p>Email: " . $email;
            
        }
        
        if (isset($_REQUEST['fecha_despacho']))
        {
            $fecha_despacho = $_REQUEST['fecha_despacho'];
            $_SESSION['fecha_despacho'] = $fecha_despacho;
            $mensaje_destacado = "Fecha entrega: " . $fecha_despacho;
        }
        
        if (isset($_REQUEST['comentarios']) && $_REQUEST['comentarios'] != "")
        {
            $comentarios = $_REQUEST['comentarios'];
            $_SESSION['comentarios'] = $comentarios;
            $comentarios_texto = "</p><p>" . $comentarios;
            
        }
        
        $mensaje_direccion = $direccion . " " . $numero . " " . $comuna_texto . " " . $telefono_texto . " " . $email_texto . " "  . $comentarios_texto;
        
        
    }
    else if ($tipo == "bodega")
    {
        $tipo_mensaje = "Retiro en Bodega";
        
        if (isset($_REQUEST['fecha_retiro']))
        {
            $fecha_retiro = $_REQUEST['fecha_retiro'];
            $_SESSION['fecha_retiro'] = $fecha_retiro;
            $mensaje_destacado = "Fecha retiro: " . $fecha_retiro;
        }else{
        	$fecha_retiro = $config['periodo_despachos'];
            $_SESSION['fecha_retiro'] = $fecha_retiro;
            $mensaje_destacado = "Fecha retiro: " . $fecha_retiro;
        }
    }
    else if ($tipo == "sucursal")
    {
        $tipo_mensaje = "Entrega en Sucursal";
        if (isset($_REQUEST['sucursal']))
        {
            list($sucursal_id,$sucursal_nombre) = explode("|",$_REQUEST['sucursal']);
            $_SESSION['sucursal_id'] = $sucursal_id;
            $mensaje_destacado = "Sucursal: " . $sucursal_nombre;

            $fecha_retiro = $config['periodo_despachos'];
            $_SESSION['fecha_retiro'] = $fecha_retiro;
            $mensaje_direccion = "Fecha entrega: " . $fecha_retiro;
        }        
    }
}


$caja_seleccionada = "";
$caja_seleccionada_mensaje = "";

if (isset($_SESSION['caja_seleccionada']))
{
    $caja_seleccionada = $_SESSION['caja_seleccionada'];
    if ($caja_seleccionada == "1")
    {
        $caja_seleccionada_mensaje = "Caja Clásica";
    }
    else if ($caja_seleccionada == "2")
    {
        $caja_seleccionada_mensaje = "Caja Cocktail";
    }
    else if ($caja_seleccionada == "3")
    {
        $caja_seleccionada_mensaje = "Caja Sin Alcohol";
    }
}


$titulo = "Resumen";

include_once dirname(__FILE__) ."/../templates/resumen.php";
    

?>