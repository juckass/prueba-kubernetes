<?php
include_once dirname(__FILE__) . "/../functions/general.php";
global $config;

$rut = $_SESSION['rut'];

$caja_seleccionada = "";
$set = "";

if (isset($_SESSION['caja_seleccionada']))
{
    $caja_seleccionada = $_SESSION['caja_seleccionada'];
    $set = " pri01_cajaSeleccionada= '$caja_seleccionada' "; 
    $_SESSION['yaTieneCajaSeleccionada'] = $caja_seleccionada; /* Esta variable sirve para detectar que cliente ya tiene una caja seleccionada y no la puede cambiar */
    
}

$tipo = "";
if (isset($_SESSION['tipo']) && $_SESSION['tipo'] != "")
{
    $tipo = $_SESSION['tipo'];
    $set .= " ,pri01_tipoDespacho= '$tipo' ";
}


$fecha_retiro = "";
if (isset($_SESSION['fecha_retiro'])  && $_SESSION['fecha_retiro'] != "")
{
    $fecha_retiro = $_SESSION['fecha_retiro'];
}
$set .= " ,pri01_retiro_fecha= '$fecha_retiro' ";


$sucursal_id = "0";
if (isset($_SESSION['sucursal_id'])  && $_SESSION['sucursal_id'] != "")
{
    $sucursal_id = $_SESSION['sucursal_id'];
}
$set .= " ,sucursal_id = '$sucursal_id' ";


$direccion = "";
if (isset($_SESSION['direccion'])  && $_SESSION['direccion'] != "")
{
    $direccion = $_SESSION['direccion'];
}
$set .= " ,pri01_despacho_direccion = '$direccion' ";


$numero = "";
if (isset($_SESSION['numero'])  && $_SESSION['numero'] != "")
{
    $numero = $_SESSION['numero'];
}
$set .= " ,pri01_despacho_numero = '$numero' ";

$comuna_id = "0";
if (isset($_SESSION['comuna_id'])  && $_SESSION['comuna_id'] != "")
{
    $comuna_id = $_SESSION['comuna_id'];
}
$set .= " ,comuna_id = $comuna_id ";


$telefono = "";
if (isset($_SESSION['telefono'])  && $_SESSION['telefono'] != "")
{
    $telefono = $_SESSION['telefono'];
}
$set .= " ,pri01_despacho_telefono = '$telefono' ";


$email = "";
if (isset($_SESSION['email'])  && $_SESSION['email'] != "")
{
    $email = $_SESSION['email'];
}
$set .= " ,pri01_despacho_email = '$email' ";

$fecha_despacho = "";
if (isset($_SESSION['fecha_despacho'])  && $_SESSION['fecha_despacho'] != "")
{
    $fecha_despacho = $_SESSION['fecha_despacho'];
}
$set .= " ,pri01_depacho_fecha = '$fecha_despacho' ";

$comentarios = "";
if (isset($_SESSION['comentarios'])  && $_SESSION['comentarios'] != "")
{
    $comentarios = $_SESSION['comentarios'];
}
$set .= " ,pri01_despacho_comentarios = '$comentarios' ";


if ($set != "")
{
    $stmt = "
        UPDATE
            pri01_usuarios
        SET
            $set
        WHERE
            pri01_rut = $rut
    ";

    $sth = execstmt($config['conn'],$stmt);

    // Ver si ya hay un pedido
    
    $stmt = "
    SELECT
        count(1) as contador
    FROM 
        pri04_pedidos
    WHERE
        pri04_pedidos.pri01_rut = $rut
    ";
    $sth = execstmt($config['conn'],$stmt);
    
    $contador = 0;
    if ($res = mysql_fetch_array($sth))
    {
        $contador =  $res['contador'];

        if($contador == 0){
            $stmt = "
            INSERT INTO pri04_pedidos (pri01_rut,pri05_id_estado) VALUES ($rut,1)
            ";
        
            execstmt($config['conn'],$stmt);
        }
            

        $stmt = "SELECT
            pri01_usuarios.pri01_rut,
            pri01_usuarios.pri01_dv,
            pri01_usuarios.pri01_apePaterno,
            pri01_usuarios.pri01_apeMaterno,
            pri01_usuarios.pri01_nombre,
            pri01_usuarios.pri01_email,
            pri02_empresas.pri02_idEmpresa,
            pri02_empresas.pri02_empresa,
            pri01_usuarios.pri01_unidad,
            pri03_oficinas.pri03_idOficina,
            pri03_oficinas.pri03_oficina,
            pri03_oficinas.pri03_region,
            pri01_usuarios.pri01_cajaSeleccionada,
            pri01_usuarios.pri01_tipoDespacho,
            pri01_usuarios.pri01_despacho_direccion,
            pri01_usuarios.pri01_despacho_numero,
            comunas.comuna_nombre,
            pri01_usuarios.pri01_despacho_telefono,
            pri01_usuarios.pri01_despacho_email,
            pri01_usuarios.pri01_depacho_fecha,
            pri01_usuarios.pri01_retiro_fecha,
            oficina_despacho.pri03_oficina as oficinaDespacho,
            pri01_usuarios.pri01_despacho_comentarios,
            pri04_pedidos.pri04_id_pedido,
            pri05_estado_pedidos.pri05_nombreEstado,
            oficina_despacho.pri03_direccion,
            regiones.region_nombre
        FROM
            pri01_usuarios
        INNER JOIN pri02_empresas ON (pri02_empresas.pri02_idEmpresa = pri01_usuarios.pri02_idEmpresa)
        INNER JOIN pri03_oficinas ON (pri03_oficinas.pri03_idOficina = pri01_usuarios.pri03_idOficina)
        LEFT JOIN comunas ON (comunas.comuna_id = pri01_usuarios.comuna_id)
        LEFT JOIN pri03_oficinas AS oficina_despacho ON (pri01_usuarios.sucursal_id = oficina_despacho.pri03_idOficina)
        LEFT JOIN pri04_pedidos ON (pri04_pedidos.pri01_rut = pri01_usuarios.pri01_rut)
        LEFT JOIN pri05_estado_pedidos ON (pri04_pedidos.pri05_id_estado = pri05_estado_pedidos.pri05_id_estado)
        LEFT JOIN regiones ON regiones.region_numero = pri03_oficinas.pri03_region
        WHERE
            pri01_usuarios.pri01_rut = '" . $rut . "';";
        $sth = execstmt($config['conn'],$stmt);
        $datosUsuario = mysql_fetch_array($sth);
        if(empty($email))
        {
            $email = $datosUsuario['pri01_email'];
        
        }

        if(!empty($email))
        {

            $de = "info@prilogic.cl";
            $para = $email;
            $asunto = 'Comprobante de pedido';
            $cuerpo = utf8_decode('<div class="comprobante" style="display: block;">');
                        if($_SESSION['modificacion_despacho'] == 'si'){
                            $cuerpo .= utf8_decode('<p style="text-align: left;margin-left: 10%;">
                            Estimado ' . $datosUsuario['pri01_nombre']. ' ' . $datosUsuario['pri01_apePaterno'] . ', usted ha cambiado la direcci&oacute;n de entrega y tiene un (1) pedido con el siguiente detalle :
                            </p>
                            <br>');
                        }else{
                            $cuerpo .= utf8_decode('<p style="text-align: left;margin-left: 10%;">
                            Estimado ' . $datosUsuario['pri01_nombre']. ' ' . $datosUsuario['pri01_apePaterno'] . ', usted tiene un (1) pedido con el siguiente detalle :
                            </p>
                            <br>');
                        }
                        
                        
                        $cuerpo.= utf8_decode('<p style="text-align: left; margin-left: 20%;">
                        Nro Pedido : ' . $datosUsuario['pri04_id_pedido'] . '<br>
                        Caja : ' . $config['cajaSeleccionada'][$datosUsuario['pri01_cajaSeleccionada']] . '<br>
                        Tipo de despacho : ' . $datosUsuario['pri01_tipoDespacho'] . '<br>');
			if($datosUsuario['pri01_tipoDespacho'] == 'bodega'){
				$cuerpo .= utf8_decode('Fecha de retiro : ' .  $_SESSION['fecha_retiro'] . '<br>');
			}

                        if($datosUsuario['pri01_tipoDespacho'] == 'domicilio')
                        {
                            $cuerpo .= utf8_decode('Dirección : ' . $datosUsuario['pri01_despacho_direccion'] . '<br>
                            Nro : ' . $datosUsuario['pri01_despacho_numero'] . '<br>
                            Comuna : ' . utf8_encode($datosUsuario['comuna_nombre']) . '<br>
                            Tel&eacute;fono : ' . $datosUsuario['pri01_despacho_telefono'] . '<br>
                            Fecha entrega : ' . $datosUsuario['pri01_depacho_fecha'] . '<br>
                            Comentario : ' . $datosUsuario['pri01_despacho_comentarios'] . '</p><br>');
                        }elseif ($datosUsuario['pri01_tipoDespacho'] == 'sucursal'){
                            $cuerpo .= utf8_decode('Oficina : ' . $datosUsuario['oficinaDespacho'] . '<br>
                            Regi&oacute;n : ' . $datosUsuario['region_nombre'] . '<br>
                            Direcci&oacute;n : ' . $datosUsuario['pri03_direccion'] . '<br>
                            Fecha entrega : ' . $datosUsuario['pri01_retiro_fecha'] . '</p><br>');
                        }

                        $cuerpo .= utf8_decode('<p style="text-align: left;margin-left: 10%;">
                            Saludos.
                        </p>
                    </div>');
            email_html($de, $para, $asunto, $cuerpo);
            echo '<form id="comprobante" method="post" action="index.php">
                    <input type="hidden" name="op" value="comprobante">
                    <input type="hidden" name="msj" value="Se ha enviado su comprobante al correo indicado.">
                </form>';
            echo '<script type="text/javascript">
                    $(document).ready(function(){
                        $("#comprobante").submit();
                    });
                </script>';
            exit;
        }elseif( empty($datosUsuario['pri01_email'])){
            echo '<form id="comprobante" method="post" action="index.php">
                    <input type="hidden" name="op" value="comprobante">
                    <input type="hidden" name="msj" value="Usted no tiene un email registrado">
                </form>';
            echo '<script type="text/javascript">
                    $(document).ready(function(){
                        $("#comprobante").submit();
                    });
                </script>';
            exit;
        }
        
    }
}

//header("Location: index.php?op=cuenta");    

?>