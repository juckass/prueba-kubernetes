<?php
global $config;
mb_internal_encoding('UTF-8');
 
function validutf8($texto){

    if(mb_detect_encoding($texto) == 'UTF8')
    {
        $producto_name = utf8_decode($texto);
    }else{
        $producto_name = utf8_encode($texto);
    }
    return $producto_name;
}

$getData = 0;
$start = 0;
$length = 10;
$draw = 1;
$excel = 0;
$order_column = "";
$order_dir = "asc";
$tipo = "total";

if (isset($_REQUEST['tipo']))
{
    $tipo = $_REQUEST['tipo'];

}


if (isset($_REQUEST['getData']))
{
    $getData = $_REQUEST['getData'];
    
}

if (isset($_REQUEST['start']))
{
    $start = $_REQUEST['start'];

}
        
if (isset($_REQUEST['length']))
{
    $length = $_REQUEST['length'];

}

if (isset($_REQUEST['draw']))
{
    $draw = $_REQUEST['draw'];

}

if (isset($_REQUEST['excel']))
{
    $excel = $_REQUEST['excel'];

}

if (isset($_REQUEST['order']))
{
    $order = $_REQUEST['order'];
    
    if (isset($order[0]['column']))
    {
        $order_column = $order[0]['column'];
    }

    if (isset($order[0]['dir']))
    {
        $order_dir = $order[0]['dir'];
    }
    
}


if ($getData == 1)
{

    $array_campos = array(
        "0" => "pri01_usuarios.pri01_rut", 
        "1" => "pri01_usuarios.pri01_apePaterno",
        "2" => "pri01_usuarios.pri01_apeMaterno",
        "3" => "pri01_usuarios.pri01_nombre",
        "4" => "pri02_empresas.pri02_idEmpresa",
        "5" => "pri02_empresas.pri02_empresa",
        "6" => "pri01_usuarios.pri01_unidad",
        "7" => "pri03_oficinas.pri03_idOficina",
        "8" => "pri03_oficinas.pri03_oficina",
        "9" => "pri01_usuarios.pri01_rut",
        "10" => "pri01_usuarios.pri01_cajaSeleccionada",
        "11" => "pri01_usuarios.pri01_tipoDespacho"
    );
    
    $stmt = "
    select
    	pri01_usuarios.pri01_rut,
    	pri01_usuarios.pri01_dv,
    	pri01_usuarios.pri01_apePaterno,
    	pri01_usuarios.pri01_apeMaterno,
    	pri01_usuarios.pri01_nombre,
        pri02_empresas.pri02_idEmpresa,
    	pri02_empresas.pri02_empresa,
    	pri01_usuarios.pri01_unidad,
        pri03_oficinas.pri03_idOficina,
    	pri03_oficinas.pri03_oficina,
    	pri03_oficinas.pri03_region,
    	pri01_usuarios.pri01_cajaSeleccionada,
        pri04_pedidos.pri04_id_pedido,
        pri05_estado_pedidos.pri05_nombreEstado,
    	pri01_usuarios.pri01_tipoDespacho,
    	pri01_usuarios.pri01_despacho_direccion,
        pri01_usuarios.pri01_despacho_numero,
        comunas.comuna_nombre,
        pri01_usuarios.pri01_despacho_telefono,
        pri01_usuarios.pri01_despacho_email,
        pri01_usuarios.pri01_depacho_fecha,
        pri01_usuarios.pri01_retiro_fecha,
        oficinas.pri03_oficina as oficina_despacho,
        pri01_usuarios.pri01_despacho_comentarios,
        oficinas.pri03_direccion        
        	
    from 
    	pri01_usuarios
    	inner join pri02_empresas ON (pri02_empresas.pri02_idEmpresa = pri01_usuarios.pri02_idEmpresa )
    	inner join pri03_oficinas ON (pri03_oficinas.pri03_idOficina = pri01_usuarios.pri03_idOficina)
        left join comunas ON (comunas.comuna_id = pri01_usuarios.comuna_id)
        left join pri03_oficinas as oficinas ON (pri01_usuarios.sucursal_id = oficinas.pri03_idOficina)
        left join pri04_pedidos ON (pri04_pedidos.pri01_rut = pri01_usuarios.pri01_rut)
        left join pri05_estado_pedidos ON (pri04_pedidos.pri05_id_estado = pri05_estado_pedidos.pri05_id_estado)
        
    ";

    if ($tipo == "no_han_realizado")
    {
        $stmt .= " WHERE (pri01_usuarios.pri01_cajaSeleccionada = '' OR pri01_usuarios.pri01_cajaSeleccionada is NULL)
        ";
    }   
    else if ($tipo == "si_han_realizado")
    {
        $stmt .= " WHERE (pri01_usuarios.pri01_cajaSeleccionada != '' )
        ";
        
    } 
    else
    {
        /* Si es total , o un caso desconocido , entonces no hay filtro */
    }
    
    if ($order_column != "")
    {
        $stmt .= "ORDER BY $array_campos[$order_column] $order_dir";    
    
    }
    
    
    $tablaDatos = array();
    
    $sth = execstmt($config['conn'],$stmt);

    $numero_filas = mysql_num_rows($sth);
    
    if ($length == -1 || $excel == 1 )
    {
        $length = $numero_filas;
    }
    
    
    $resultado = '';
    
    if ($excel == 0)
    {
        $resultado .= '{
            "draw" : '. $draw .',
            "recordsTotal" : ' . $numero_filas .',
            "recordsFiltered": ' . $numero_filas . ',
        ';
        
        if ($numero_filas > 0)
        {
            $resultado .= ' "data": [ ';
        }
    }
    else 
    {
        $resultado .= '[';
    }
    
    $contador_registro = 0;
    $contador_dato = 0;
    while ($res = mysql_fetch_array($sth))
    {
        $contador_registro++;
        
        if ($contador_registro > ($start))
        {
            $contador_dato++;
            
            if ($excel == 0)
            {
            
                $resultado .= '
                        [
                        "' .$res['pri01_rut'] . '-' .$res['pri01_dv'] .'",
                        "' .$res['pri01_apePaterno'] .'",
                        "' .$res['pri01_apeMaterno'] .'",
                        "' .$res['pri01_nombre'] .'",
                        "' .$res['pri02_idEmpresa'] .'",
                        "' .$res['pri02_empresa'] .'",
                        "' .$res['pri01_unidad'] .'",
                        "' .$res['pri03_idOficina'] .'",
                        "' .$res['pri03_oficina'] .'",
                        "' .$config['cajaSeleccionada'][$res['pri01_cajaSeleccionada']] .'",
                        "' .$res['pri01_tipoDespacho'] .'"
                        ]
                ';
            }
            else 
            {
                $caja_seleccionada = "";
                if (isset($config['cajaSeleccionada'][$res['pri01_cajaSeleccionada']]))
                {
                    $caja_seleccionada = $config['cajaSeleccionada'][$res['pri01_cajaSeleccionada']];
                }
                
                $comentarios = "";
                if (isset($res['pri01_despacho_comentarios']))
                {
                    $comentarios = str_replace("\""," ",$res['pri01_despacho_comentarios']);
                    $comentarios = str_replace("'"," ",$comentarios);
                    $comentarios = str_replace("\n"," ",$comentarios);
                    $comentarios = str_replace("\r"," ",$comentarios);
                    
                }
                
                
               $resultado .= '
                        {
                        "Rut" : "' .  validutf8($res['pri01_rut'] . '-' .$res['pri01_dv']) .'",
                        "Apellido Paterno" :"' .  validutf8($res['pri01_apePaterno']) .'",
                        "Apellido Materno" :"' .  validutf8($res['pri01_apeMaterno']) .'",
                        "Nombre" :"' .  validutf8($res['pri01_nombre']) .'",
                        "Empresa" :"' .  validutf8($res['pri02_idEmpresa']) .'",
                        "Gls_Empresa" :"' .  validutf8($res['pri02_empresa']) .'",
                        "Gls_Unidad" :"' .  validutf8($res['pri01_unidad']) .'",
                        "Oficina" :"' .  validutf8($res['pri03_idOficina']) .'",
                        "Gls_Oficina" :"' .  validutf8($res['pri03_oficina']) .'",
                        "Caja Seleccionada ID" :"' .  validutf8($res['pri01_cajaSeleccionada']) .'",
                        "Caja Seleccionada" :"' .  validutf8($caja_seleccionada) .'",
                        "Numero de Pedido" :"' .  validutf8($res['pri04_id_pedido']) .'",
                        "Estado del Pedido" :"' .  validutf8($res['pri05_nombreEstado']) .'",
                        "Tipo Despacho" :"' .  validutf8($res['pri01_tipoDespacho']) .'",
                        "Direccion Despacho" :"' . validutf8($res['pri01_despacho_direccion']) .'",
                        "Numero" :"' .  validutf8($res['pri01_despacho_numero']) .'",
                        "Comuna" :"' . validutf8($res['comuna_nombre']) .'",
                        "Telefono" :"' .  validutf8($res['pri01_despacho_telefono']) .'",
                        "Email" :"' .  validutf8($res['pri01_despacho_email']) .'",
                        "Fecha Solicitud de Entrega" :"' .  validutf8($res['pri01_depacho_fecha']) .'",
                        "Comentarios" :"' . $comentarios .'",
                        "Retiro Fecha" :"' .  validutf8($res['pri01_retiro_fecha']) .'",
                        "Nombre Sucursal" :"' . validutf8($res['oficina_despacho']) .'",
                        "Direccion Sucursal" :"' . validutf8($res['pri03_direccion']) .'"
                        }
                ';
                
            }
            
            
            if ($contador_dato < $length &&  $contador_registro < $numero_filas)
            {
                $resultado .= ",";
            }
            else 
            {
                break;
            }
        }        
    }

    if ($excel == 0)
    {

        if ($numero_filas > 0)
        {
            $resultado .= " ] ";
        }
        
        $resultado .= "}";
        ob_clean();
        print $resultado;
        exit;
        
    }
    else
    {
        $resultado .= ']';
        
        ob_clean();
    
        /* Generar el excel */
        
        require(dirname(__FILE__)  .'/../library/PHPExcel.php');
        require_once dirname(__FILE__)  ."/../library/PHPExcel/IOFactory.php";
        $file1=$resultado;

        $array=json_decode($file1);
        
        /*
    switch (json_last_error()) {
        case JSON_ERROR_NONE:
            echo ' - No errors';
        break;
        case JSON_ERROR_DEPTH:
            echo ' - Maximum stack depth exceeded';
        break;
        case JSON_ERROR_STATE_MISMATCH:
            echo ' - Underflow or the modes mismatch';
        break;
        case JSON_ERROR_CTRL_CHAR:
            echo ' - Unexpected control character found';
        break;
        case JSON_ERROR_SYNTAX:
            echo ' - Syntax error, malformed JSON';
        break;
        case JSON_ERROR_UTF8:
            echo ' - Malformed UTF-8 characters, possibly incorrectly encoded';
        break;
        default:
            echo ' - Unknown error';
        break;
    }        
        
        */
        
        ini_set('memory_limit', '-1');
        $objXLS =new PHPExcel();
        $value=1;
        $man_val=array();
        //set the heading for first time
        
        foreach ($array as $key => $jsons) {
            foreach($jsons as $key => $value1) {

                $value1 = validutf8($value1);
                
                //$value1 = html_entity_decode($value1,ENT_QUOTES | ENT_IGNORE, "UTF-8");
                                
                array_push($man_val,$key);
            }
            break;
        }
        $objXLS->getSheet(0)->fromArray($man_val, null, "A".$value);
        
        $man_val=array();
        $value=2;
        foreach ($array as $key => $jsons) {
        
            foreach($jsons as $key => $value1) {
                array_push($man_val,$value1);
            }
            $objXLS->getSheet(0)->fromArray($man_val, null, "A".$value);
            $value=$value+1;
            $man_val=array();
        }
        
        
        $fileName = "datos";
        
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'. $fileName . '.xlsx"');
        header('Cache-Control: max-age=0');
    
        /*
        $fileType = 'Excel2007';
        //$fileType =' Excel5';
            
        $objWriter = PHPExcel_IOFactory::createWriter($objXLS, $fileType);
        $objWriter->save('php://output');
        */
        
        $objWriter = new PHPExcel_Writer_Excel2007($objXLS);
        $objWriter->save('php://output');
        //$objWriter->save(str_replace('.php', '.xlsx', __FILE__));
        exit;    
    }    
}
else {
    include_once dirname(__FILE__) ."/../templates/administrador.php";
}
    

    

?>