<?php
include_once dirname(__FILE__) . "/../functions/general.php";
global $config;

$op = $_REQUEST['op'];

switch ($op) {
	case 'mail':
			ob_clean();
			$respuesta['status'] = 1;
			$de = "info@prilogic.cl";
			$para = $_REQUEST['mail'];
			$asunto = $_REQUEST['asunto'];
			$cuerpo = $_REQUEST['cuerpo'];
			if(trim($para) != "")
			{
				email_html($de, $para, $asunto, $cuerpo);
				$respuesta['msj'] = "Se envió en comprobante al su correo";
			}else{
				$respuesta['status'] = 2;
				$respuesta['msj'] = "Usted no tiene un email registrado";
			}
			echo json_encode($respuesta);
			exit;
		break;
	case 'reenvio':
			$stmt = "SELECT
				pri01_usuarios.pri01_rut,
				pri01_usuarios.pri01_dv,
				pri01_usuarios.pri01_apePaterno,
				pri01_usuarios.pri01_apeMaterno,
				pri01_usuarios.pri01_nombre,
				pri01_usuarios.pri01_email,
				pri02_empresas.pri02_idEmpresa,
				pri02_empresas.pri02_empresa,
				pri01_usuarios.pri01_unidad,
				pri03_oficinas.pri03_idOficina,
				pri03_oficinas.pri03_oficina,
				pri03_oficinas.pri03_region,
				pri01_usuarios.pri01_cajaSeleccionada,
				pri01_usuarios.pri01_tipoDespacho,
				pri01_usuarios.pri01_despacho_direccion,
				pri01_usuarios.pri01_despacho_numero,
				comunas.comuna_nombre,
				pri01_usuarios.pri01_despacho_telefono,
				pri01_usuarios.pri01_despacho_email,
				pri01_usuarios.pri01_depacho_fecha,
				pri01_usuarios.pri01_retiro_fecha,
				oficina_despacho.pri03_oficina as oficinaDespacho,
				pri01_usuarios.pri01_despacho_comentarios,
				pri04_pedidos.pri04_id_pedido,
				pri05_estado_pedidos.pri05_nombreEstado,
				oficina_despacho.pri03_direccion,
				regiones.region_nombre
			FROM
				pri01_usuarios
			INNER JOIN pri02_empresas ON (pri02_empresas.pri02_idEmpresa = pri01_usuarios.pri02_idEmpresa)
			INNER JOIN pri03_oficinas ON (pri03_oficinas.pri03_idOficina = pri01_usuarios.pri03_idOficina)
			LEFT JOIN comunas ON (comunas.comuna_id = pri01_usuarios.comuna_id)
			LEFT JOIN pri03_oficinas AS oficina_despacho ON (pri01_usuarios.sucursal_id = oficina_despacho.pri03_idOficina)
			LEFT JOIN pri04_pedidos ON (pri04_pedidos.pri01_rut = pri01_usuarios.pri01_rut)
			LEFT JOIN pri05_estado_pedidos ON (pri04_pedidos.pri05_id_estado = pri05_estado_pedidos.pri05_id_estado)
			LEFT JOIN regiones ON regiones.region_numero = pri03_oficinas.pri03_region
			WHERE
				pri01_usuarios.pri01_rut = '" . $_SESSION['rut'] . "';";
			$sth = execstmt($config['conn'],$stmt);
			$datosUsuario = mysql_fetch_array($sth);

			include_once dirname(__FILE__) ."/../templates/reenvio.php";
			exit;
		break;
	case 'comprobante':
			$stmt = "SELECT
				pri01_usuarios.pri01_rut,
				pri01_usuarios.pri01_dv,
				pri01_usuarios.pri01_apePaterno,
				pri01_usuarios.pri01_apeMaterno,
				pri01_usuarios.pri01_nombre,
				pri01_usuarios.pri01_email,
				pri02_empresas.pri02_idEmpresa,
				pri02_empresas.pri02_empresa,
				pri01_usuarios.pri01_unidad,
				pri03_oficinas.pri03_idOficina,
				pri03_oficinas.pri03_oficina,
				pri03_oficinas.pri03_region,
				pri01_usuarios.pri01_cajaSeleccionada,
				pri01_usuarios.pri01_tipoDespacho,
				pri01_usuarios.pri01_despacho_direccion,
				pri01_usuarios.pri01_despacho_numero,
				comunas.comuna_nombre,
				pri01_usuarios.pri01_despacho_telefono,
				pri01_usuarios.pri01_despacho_email,
				pri01_usuarios.pri01_depacho_fecha,
				pri01_usuarios.pri01_retiro_fecha,
				oficina_despacho.pri03_oficina as oficinaDespacho,
				pri01_usuarios.pri01_despacho_comentarios,
				pri04_pedidos.pri04_id_pedido,
				pri05_estado_pedidos.pri05_nombreEstado,
				oficina_despacho.pri03_direccion,
				regiones.region_nombre
			FROM
				pri01_usuarios
			INNER JOIN pri02_empresas ON (pri02_empresas.pri02_idEmpresa = pri01_usuarios.pri02_idEmpresa)
			INNER JOIN pri03_oficinas ON (pri03_oficinas.pri03_idOficina = pri01_usuarios.pri03_idOficina)
			LEFT JOIN comunas ON (comunas.comuna_id = pri01_usuarios.comuna_id)
			LEFT JOIN pri03_oficinas AS oficina_despacho ON (pri01_usuarios.sucursal_id = oficina_despacho.pri03_idOficina)
			LEFT JOIN pri04_pedidos ON (pri04_pedidos.pri01_rut = pri01_usuarios.pri01_rut)
			LEFT JOIN pri05_estado_pedidos ON (pri04_pedidos.pri05_id_estado = pri05_estado_pedidos.pri05_id_estado)
			LEFT JOIN regiones ON regiones.region_numero = pri03_oficinas.pri03_region
			WHERE
				pri01_usuarios.pri01_rut = '" . $_SESSION['rut'] . "';";
			$sth = execstmt($config['conn'],$stmt);
			$datosUsuario = mysql_fetch_array($sth);

			include_once dirname(__FILE__) ."/../templates/comprobante.php";
			exit;
		break;
	
	default:
			// nada
		break;
}
