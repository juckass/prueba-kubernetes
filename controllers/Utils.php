<?php

class Utilidades
{
    public static $instance;
    private $csrf = null;

    private function __construct()
    {
        /* PRIVATE */
    }

    public static function getInstance()
    {
        Utilidades::$instance = new Utilidades();
        
        return Utilidades::$instance;
    }

    public function validaRut($rut)
    {
        if (!preg_match("/^[0-9.]+[-]?+[0-9kK]{1}/", $rut)) {
            return false;
        }

        $rut = preg_replace('/[\.\-]/i', '', $rut);
        $dv = substr($rut, -1);
        $numero = substr($rut, 0, strlen($rut) - 1);
        $i = 2;
        $suma = 0;
        
        foreach (array_reverse(str_split($numero)) as $v) {
            if ($i == 8) {
                $i = 2;
            }
                
            $suma += $v * $i;
            ++$i;
        }

        $dvr = 11 - ($suma % 11);
        
        if ($dvr == 11) {
            $dvr = 0;
        }
            
        if ($dvr == 10) {
            $dvr = 'K';
        }

        return ($dvr == strtoupper($dv));
    }

    public function limpiarParametro($param)
    {
        return mysql_real_escape_string($param);
    }

    public function generarCSRF()
    {
        if (empty($_SESSION['token'])) {
            if (function_exists('mcrypt_create_iv')) {
                $this->csrf = bin2hex(mcrypt_create_iv(32, MCRYPT_DEV_URANDOM));
            } else {
                $this->csrf = bin2hex(openssl_random_pseudo_bytes(32));
            }

            $_SESSION['token'] = $this->csrf;
        } else {
            $this->csrf = $_SESSION['token'];
        }
    }

    public function getCSRF()
    {
        return $this->csrf;
    }

    public function checkCSRF($token)
    {
        if (!empty($token)) {
            return (hash_equals($_SESSION['token'], $token));
        } else {
            return false;
        }
    }

    public function DBConnect($host, $db, $username, $password)
    {
        $dsn= "mysql:host=$host;dbname=$db";
        
        try {
            $conn = new PDO($dsn, $username, $password);
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        
            return $conn;
        } catch (PDOException $e) {
            echo $e->getMessage();
            return null;
        }
    }

    public function DBQuery($q, $rut, $dv, $password, $config)
    {
        try {
            $dbh = $this->DBConnect(
                $config['db_host'],
                $config['db_name'],
                $config['db_user'],
                $config['db_passwd']
            );
            

            if (!empty($dbh)) {
                $sth = $dbh->prepare($q);

                $sth->bindParam(':rut', $rut);
                $sth->bindParam(':dv', $dv);
                $sth->bindParam(':password', $password);
                $sth->execute();

                return $sth->fetch();
            } else {
                return null;
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            return null;
        }
    }
}
?>
