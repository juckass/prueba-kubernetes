<?php
global $config;




/*
ob_clean();
print "<pre>";
print_r($_SESSION);
print "<hr>";
print_r($_REQUEST);
print "</pre>";
exit;
*/

if (!isset($_REQUEST['op']) || !isset($_REQUEST['rut']))
{
    include_once dirname(__FILE__) ."/../templates/recoverpw.php";
    exit;
}

//$rut = mysql_real_escape_string($_REQUEST['rut']);

$rut = $_REQUEST['rut'];

list($rut_tmp,$dv_tmp) = explode("-",$rut);
$rut_tmp = str_replace(".", "", $rut_tmp);


if(!preg_match("/^[0-9]+$/", $rut_tmp))
{ 
    header("Location: " .  $config['base_url'] . "index.php?op=recoverpw");
    exit;
}

if(count($dv_tmp) != 1){
    header("Location: " .  $config['base_url'] . "index.php?op=recoverpw");
    exit;
}

if(!preg_match("/^[0-9 kK]+$/", $dv_tmp))
{ 

    header("Location: " .  $config['base_url'] . "index.php?op=recoverpw");
    exit;
}

$rut_tmp = strtoupper($rut_tmp);
$dv_tmp = strtoupper($dv_tmp);

/*echo var_dump($_REQUEST['$rut_tmp']);
exit();*/

//Realizar busqueda en base de datos del rut

$stmt = "SELECT
            pri01_rut,
            pri01_email
        FROM
            pri01_usuarios
        WHERE
            pri01_rut = '".$rut_tmp."'
            AND pri01_dv = '".$dv_tmp."'";

$sth = execstmt($config['conn'],$stmt);

if ($res = mysql_fetch_array($sth))
{
    $rut_db = $res['pri01_rut'];
    $email_db = $res['pri01_email'];
}

if($rut_db == "")
{
    header("Location: " .  $config['base_url'] . "index.php?op=recoverpw&error=1");
    exit;
}
elseif($email_db == "")
{
    header("Location: " .  $config['base_url'] . "index.php?op=recoverpw&error=3");
    exit;
}
else //Enviar correo
{
    if(sendEmail($rut, $email_db)){
        header("Location: " .  $config['base_url'] . "index.php?op=recoverpw&mensaje=1");
    }
    else{
        header("Location: " .  $config['base_url'] . "index.php?op=recoverpw&error=2");
    }
}


function sendEmail($rut, $email_db){
    $url = "http://santander.prilogic.cl/index.php?op=changepw&sol=";
    $url= $url.urlencode(encrypt($rut));
    $from = "no-reply@prilogic.cl";
    $to = /*"fquijada@netred.cl"*/$email_db;
    $subject = "Recuperar contraseña Prilogic";

    $message = "<html><body> Estimado(a) <b>Administrador</b>:<br>";
    $message.= "Se ha solicitado recuperar la contrase&ntilde;a con fecha ".date("Y-m-d")." a las ".date("H:i")."<br>";
    $message.= "Login: ".$rut."<br>";
    $message.= "para recuperar la contrase&ntilde;a hacer click en el link: <a href='".$url."'>".$url."<a/><br>";
    $message.= "Atte.<br> Prilogic.cl<br>(*) Este Mensaje ha sido generado de forma autom&aacute;tica, favor no responder. </body></html>";

    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    $headers .= "From:" . $from;
    $email = mail($to,$subject,$message, $headers);
    if (!$email) {
        return false;
    }
    else {
        return true;
    }
}




/*if ($password1 == $password2)
{

    $stmt = "
        UPDATE
            pri01_usuarios
        SET
            pri01_password = md5('$password1')
        WHERE
            pri01_rut = $rut_tmp
    ";

    $sth = execstmt($config['conn'],$stmt);


    header("Location: " .  $config['base_url'] . "index.php?op=elige-caja");
    exit;
}
else
{

    header("Location: " .  $config['base_url'] . "index.php?op=changepw&error=1");

}*/

?>