<?php
	ob_clean();

	global $config;

	$region_id = "0";

	if (isset($_REQUEST['region_id']))
	{
		$region_id = $_REQUEST['region_id'];
	}

	if ($region_id == "" || $region_id == "0")
	{
		exit;
	}

	$stmt = "
	SELECT 
		pri03_oficinas.pri03_idOficina,
		pri03_oficinas.pri03_oficina,
		pri03_oficinas.pri03_region,
		pri03_oficinas.pri03_direccion
	from
		pri03_oficinas
	where
		pri03_oficinas.pri03_region = $region_id
	ORDER BY
		pri03_oficinas.pri03_oficina
	";

	$sth = execstmt($config['conn'],$stmt);
?>
<p class="text-red form-title">Sucursales de <span id="sucursalName"></span></p>
<form id="elige-sucursal" method="post" action="index.php">
	<input type="hidden" name="op" value="resumen">
	<input type="hidden" name="tipo" value="sucursal" />
	<div id="results" class="bordes" style="border:solid 3px; border-color: #e63d3a;">
		<?php
			while ($res = mysql_fetch_array($sth))
			{
				$tmp_direccion = print "
				<div class='col-xs-12 col-sm-12 col-md-6 col-lg-6 item-list'>
					<div class='radio'>
						<label>
							<input type='radio' name='sucursal' value='". $res['pri03_idOficina'] . "|". htmlentities(utf8_encode($res['pri03_oficina'])) . " (". htmlentities(utf8_encode($res['pri03_direccion'])) . ")" . "' required='required' />" . 
								htmlentities(utf8_encode($res['pri03_oficina'])) . "<br>
								(" . htmlentities(utf8_encode($res['pri03_direccion'])) . ")" ."
						</label>
					</div>
				</div>";
			}
		?>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
		<a class="btn btn-default btn-back" title="" href="index.php?op=elige-despacho">VOLVER</a>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
		<input type="submit" class="btn btn-default" title="" value="SIGUIENTE" />
	</div>
</form>
<?php 

/*
<script>
	function CheckRequired(event) {
		var $form = $('#elige-sucursal');

		if ($form.find('[required]:visible').filter(function(){ return this.value === '' }).length > 0) {
			event.preventDefault();
			alert("One or more fields cannot be blank");
			return false;
		}
	}

	$(document).ready(function () {
		$('#elige-sucursal').on('submit', CheckRequired);
	});
</script>
*/
exit;
?>                    