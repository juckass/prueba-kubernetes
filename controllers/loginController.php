<?php
global $config;
$utils = Utilidades::getInstance();

$rut = "";
$dv = "";

if (isset($_REQUEST["rut"])) {
    $rutValido = $utils->validaRut($_REQUEST["rut"]);
    
    if ($rutValido) {
        list($rut,$dv) = explode("-", $_REQUEST["rut"]);
        $rut = str_replace(".", "", $rut);
        $rut = strtoupper($rut);
        $dv = strtoupper($dv);
    } else {
        header("Location: " .  $config['base_url'] . "index.php?error=99");
        exit;
    }
}

$password = "";
if (isset($_REQUEST["password"])) {
    $password_no_ecriptado = $_REQUEST["password"];
    $password = md5($_REQUEST["password"]);
}

if ($rut == "" || $dv == "" || $password == "") {
    header("Location: " .  $config['base_url'] . "index.php?error=1");
    exit;
}

/* 1-9 */
if ( $rut == "1" && $password_no_ecriptado == "admin99") {

    $_SESSION['rut'] = $rut;
    $_SESSION['nombre'] = "Administrador";
    $_SESSION['empresa'] = "";
    $_SESSION['tipo_administrador'] = "1";
    
    header("Location: " .  $config['base_url'] . "index.php?op=administrador");
    exit;
}

/** DESACTIVADO EL LOGEO **/
/*header("Location: " .  $config['base_url'] . "index.php?error=2");
exit;*/

$_SESSION['modificacion_despacho'] = 'no';

$rut = $utils->limpiarParametro($rut);
$dv = $utils->limpiarParametro($dv);
$password = $utils->limpiarParametro($password);

$stmt = "
    select
        pri01_usuarios.pri01_nombre as nombre,
        pri01_usuarios.pri01_apePaterno as apellido_pat,
        pri01_usuarios.pri01_apeMaterno as apellido_mat,
        pri02_empresas.pri02_empresa as empresa,
        pri01_cajaSeleccionada as cajaSeleccionada,
        Pri01_tipoDespacho as tipoDespacho,
        pri03_oficinas.pri03_region as region
    from
        pri01_usuarios
        left join pri02_empresas on (pri01_usuarios.pri02_idEmpresa = pri02_empresas.pri02_idEmpresa )
        left join pri03_oficinas on pri01_usuarios.pri03_idOficina = pri03_oficinas.pri03_idOficina
    where
        pri01_rut = :rut
        and pri01_dv = :dv
        and pri01_password = :password;";
$res = $utils->DBQuery($stmt, $rut, $dv, $password, $config);

if (!isset($config['base_url'])) {
    $config['base_url'] = "";
}
header("Location: " .  $config['base_url'] . "index.php?error=3");
    exit;
// if ($res = mysql_fetch_array($sth)) {
if (!empty($res)) {
    $hashValido = $utils->checkCSRF($_REQUEST['csrf']);
    $_SESSION['rut'] = $rut;
    $_SESSION['dv'] = $dv;
    $_SESSION['nombre'] = $res['nombre'] . " " . $res['apellido_pat'] . " " . $res['apellido_mat'];
    $_SESSION['empresa'] = $res['empresa'];
    $_SESSION['tipo_administrador'] = "0";
    $_SESSION['yaTieneCajaSeleccionada'] = $res['cajaSeleccionada'];


    if($res['region'] == 13){
        $_SESSION['lugar_despacho'] = "rm";
    }else {
        $_SESSION['lugar_despacho'] = "otras_regiones";
    }
    /*if (substr($rut, 0,4) == $password_no_ecriptado)
    {
        header("Location: " .  $config['base_url'] . "index.php?op=changepw");
        exit;
    }
    else
    {*/
    $stmt = "SELECT * FROM pri04_pedidos WHERE pri01_rut = '" . $rut . "';";
    $sth = execstmt($config['conn'], $stmt);
    $numero_filas = mysql_num_rows($sth);

    if ($numero_filas > 0) {


        if (isset($_REQUEST["modificar_despacho"]) && $_REQUEST["modificar_despacho"] == 1) {
            
            $_SESSION['caja_seleccionada'] = $res['cajaSeleccionada'];
            $_SESSION['lugar_despacho'] = "rm";
            $_SESSION['modificacion_despacho'] = 'si';

          
            header("Location: " .  $config['base_url'] . "index.php?op=elige-despacho");
            exit;
        } else {
            header("Location: " .  $config['base_url'] . "index.php?op=reenvio");
            exit;
        }
    } else {
        header("Location: " .  $config['base_url'] . "index.php?op=elige-caja");
        exit;
    }
    //}    
} else {
    header("Location: " .  $config['base_url'] . "index.php?error=1");
    exit;
}
?>