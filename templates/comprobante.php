<div class="contrainer-fluid nopad wrapper fadeIn" data-wow-delay="0.5s">

	<?php 

		$error = 0;
		if (isset($_REQUEST["error"]))
		{
			$error = $_REQUEST["error"];
		}

		if ($error == 1)
		{
			print "Los password no coinciden. Intentelo nuevamente.";
		}

		$rut_url = "";

		if(isset($_REQUEST['sol']))
		{
			$rut_url = decrypt($_REQUEST['sol']);
		}
	?>
	<div class="container" style="width: 90%; text-align: center;" id="cambio_contrasena">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 wow fadeInDown" style="" data-wow-delay="1s" >
			<div class="form-group login">
				<h2 class="text-center">Comprobante</h2>
				<input type="hidden" name="op" id="op" value="changing_pw">
				<input type="hidden" name="setpw" id="setpw" value="1">
				<input type="hidden" name="rut_pw" id="rut_pw" value="<?php print $rut_url;?>">

				<label style="color:black;font-weight:100; padding-bottom: 10px;">
				<div class="comprobante">

					<?php 
					
					if($_SESSION['modificacion_despacho'] == 'si'){
						echo  utf8_decode('<p style="text-align: left;margin-left: 10%;">
						Estimado ' . $datosUsuario['pri01_nombre']. ' ' . $datosUsuario['pri01_apePaterno'] . ', usted ha cambiado la direcci&oacute;n de entrega y tiene un (1) pedido con el siguiente detalle :
						</p>
						<br>');
					}else{
						echo  utf8_decode('<p style="text-align: left;margin-left: 10%;">
						Estimado ' . $datosUsuario['pri01_nombre']. ' ' . $datosUsuario['pri01_apePaterno'] . ', usted tiene un (1) pedido con el siguiente detalle :
						</p>
						<br>');
					}
					
					?>
				
					<p style="text-align: left; margin-left: 20%;">
					Nro Pedido : <?php echo $datosUsuario['pri04_id_pedido']; ?><br>
					Caja : <?php echo $config['cajaSeleccionada'][$datosUsuario['pri01_cajaSeleccionada']]; ?><br>
					Tipo de despacho : <?php echo $datosUsuario['pri01_tipoDespacho']; ?><br>
					<?php if($datosUsuario['pri01_tipoDespacho'] == 'domicilio'){ ?>
						Dirección : <?php echo $datosUsuario['pri01_despacho_direccion']; ?><br>
						Nro : <?php echo $datosUsuario['pri01_despacho_numero']; ?><br>
						Comuna : <?php echo utf8_encode($datosUsuario['comuna_nombre']); ?><br>
						Teléfono : <?php echo $datosUsuario['pri01_despacho_telefono']; ?><br>
						Fecha entrega : <?php echo $datosUsuario['pri01_depacho_fecha']; ?><br>
						Comentario : <?php echo $datosUsuario['pri01_despacho_comentarios']; ?></p><br>
					<?php }elseif ($datosUsuario['pri01_tipoDespacho'] == 'sucursal'){ ?>
						Oficina : <?php echo $datosUsuario['oficinaDespacho']; ?><br>
						Región : <?php echo $datosUsuario['region_nombre']; ?><br>
						Dirección : <?php echo $datosUsuario['pri03_direccion']; ?><br>
						Fecha entrega : <?php echo $datosUsuario['pri01_retiro_fecha']; ?><br>
					<?php }elseif ($datosUsuario['pri01_tipoDespacho'] == 'bodega'){ ?>
						Dirección : Las Américas 777, Cerrillos. Santiago Región Metropolitana.<br>
						Fecha entrega : <?php echo $datosUsuario['pri01_retiro_fecha']; ?><br>
					<?php } ?>

					<p style="text-align: left;margin-left: 10%;" >
						Saludos.
					</p>
				</div>

				<div class="form-group login">

					<h2 class="text-center"><?php echo $_REQUEST['msj']; ?></h2>

					<form method="post" action="index.php">
						<input type="submit" value="Continuar" class="btn btn-default">
					</form>
				</div>
		</div>
		</div>
	</div>
</div>
