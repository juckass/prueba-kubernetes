<!-- Validador RUT -->
<script src="js/jquery.rut.chileno.min.js"></script>

<div class="contrainer-fluid nopad wrapper fadeIn" data-wow-delay="0.5s">

    <?php

    $error = 0;
    if (isset($_REQUEST["error"]))
    {
        $error = $_REQUEST["error"];
    }


    $mensaje = 0;
    if (isset($_REQUEST["mensaje"]))
    {
        ?>
        <script type="text/javascript">
            $(document).ready(function(){
                $('#div_mensaje').show();
            });
        </script>
        <?php
    }
    else
    {
        ?>
        <script type="text/javascript">
            $(document).ready(function(){
                $('#div_recuperar').show();
            });
        </script>
        <?php
    }
    ?>

    <script>

        function validar_rut()
        {
            valor_rut = $("#rut").val();

            valor_rut = $.rut.formatear(valor_rut);

            if (valor_rut != "" && valor_rut != "1-9" && valor_rut != "19")
            {
                validacion = $.rut.validar(valor_rut);
                if (!validacion)
                {
                    alert('El rut: ' + $("#rut").val() + ' es incorrecto');
                    $("#rut").val("");
                }
                else
                {
                    $("#rut").val(valor_rut);
                }
            }
        }

    </script>

    <div class="container" style="display: none;" id="div_recuperar">
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 pull-right wow fadeInDown" data-wow-delay="1s">
            <div class="form-group login">
                <h2 class="text-center">Recuperar contraseña</h2>
                <form id="validacion-live" method="post" action="index.php">
                     <input type="hidden" name="op" id="op" value="recoverpword">
                    <input type="text" class="form-control" name="rut" id="rut" placeholder="Ingresa tu RUT" required="required" onchange="validar_rut();">
                    <?php
                    if ($error == 1)
                    {
                        print "<div class='alert alert-danger'>El RUT es inválido</div>";
                    }elseif($error == 2){
                        print "<div class='alert alert-danger'>Error al enviar correo de recuperación</div>";
                    }elseif($error == 3){
                        print "<div class='alert alert-danger'>Este rut no tiene correo registrado</div>";
                    }
                    ?>
                    <input type="submit" value="RECUPERAR CONTRASEÑA" class="btn btn-default">
                </form>
            </div>
        </div>
    </div>
    <div class="container" style="display: none;" id="div_mensaje">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 wow fadeInDown" data-wow-delay="1s">
            <div class="form-group login">

                <h2 class="text-center">Correo de recuperación enviado</h2>

                <form method="post" action="index.php">
                    <input type="submit" value="Continuar" class="btn btn-default">
                </form>
            </div>
        </div>
    </div>
</div>


