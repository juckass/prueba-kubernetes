

	<div class="contrainer-fluid nopad wrapper section">
	    <div class="container">
	    		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 section-title">
    				<h1>Elige tu Caja</h1>
	    		</div>
                <div class="clearfix"></div>

                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 wow fadeIn" data-wow-delay="0.5s">
                    <div class="item-box">
                        <a href="index.php?op=detalle-caja&v=1"><img src="img/caja-clasica.jpg" class="img-responsive" /></a>
                        <h3>Clásica</h3>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 wrap-btn">
                            <a class="btn btn-default" title="" href="index.php?op=detalle-caja&v=1">DETALLES</a>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 wow fadeIn" data-wow-delay="1s">
                    <div class="item-box">
                        <a href="index.php?op=detalle-caja&v=2"><img src="img/caja-cocktail.jpg" class="img-responsive" /></a>
                        <h3>Cocktail</h3>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 wrap-btn">
                            <a class="btn btn-default" title="" href="index.php?op=detalle-caja&v=2">DETALLES</a>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 wow fadeIn" data-wow-delay="1.5s">
                    <div class="item-box">
                        <a href="index.php?op=detalle-caja&v=3"><img src="img/caja-sin-alcohol.jpg" class="img-responsive" /></a>
                        <h3>Sin Alcohol</h3>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 wrap-btn">
                            <a class="btn btn-default" title="" href="index.php?op=detalle-caja&v=3">DETALLES</a>
                        </div>
                    </div>
                </div>

	    </div>
	</div>

	
	