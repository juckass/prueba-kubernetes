
    <!-- datePicker -->
    <script src="js/jquery-ui.min.js"></script>
    <link href="css/jquery-ui.min.css" rel="stylesheet">
    <script type="text/javascript">

    var unavailableDates = ["1-10-2017","8-10-2017","15-10-2017","22-10-2017","29-10-2017","5-11-2017","12-11-2017","19-11-2017","26-11-2017","3-12-2017","10-12-2017","17-12-2017"];
   

    function unavailable(date) {
      dmy = date.getDate() + "-" + (date.getMonth()+1) + "-" + date.getFullYear();
      if ($.inArray(dmy, unavailableDates) < 0) {
        return [true,"",""];
      } else {
        return [false,"",""];
      }
    }

        jQuery(document).ready(function(){
        	$.datepicker.regional['es'] = {
      			  closeText: 'Cerrar',
      			  prevText: '<Ant',
      			  nextText: 'Sig>',
      			  currentText: 'Hoy',
      			  monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
      			  monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
      			  dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
      			  dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
      			  dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
      			  weekHeader: 'Sm',
      			  dateFormat: 'dd/mm/yy',
      			  firstDay: 1,
      			  isRTL: false,
      			  showMonthAfterYear: false,
      			  yearSuffix: ''
      			};

      			$.datepicker.setDefaults($.datepicker.regional['es']);
        
            
            jQuery( "#datepicker" ).datepicker( {
            	dateFormat: "dd-mm-yy",
            	minDate: new Date(2017, 12 - 1, 1),
            	maxDate: new Date(2017, 12 - 1, 22),
            	beforeShowDay: unavailable 
            });

        });
    </script>

    
	<div class="contrainer-fluid nopad wrapper section">
	    <div class="container">
            <div class="box-detail">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 nopad gmap " >
                    <img src="img/google-maps.jpg" width="600" height="450" frameborder="0" style="border:0" class="img-responsive" />
                </div>

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 bordes" style="border: solid 3px #e63d3a;" >
                    <p class="text-red form-title">Retiro en bodegas</p>

                    <p class="form-title">
                    Las Américas 777, Cerrillos. Santiago<br/>
                    Región Metropolitana.
                    </p>
                    
                    <form method="POST" action="index.php">
                    <input type="hidden" name="op" value="resumen" />
                    <input type="hidden" name="tipo" value="bodega" />
                    <!--<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <label for="date">Fecha de retiro</label>
                        <input class="form-control" id="datepicker" name="fecha_retiro" type="text" required="required" />
                    </div>-->


                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 " >
                        <a class="btn btn-default btn-back" title="" href="index.php?op=elige-despacho">VOLVER</a>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <input type="submit" class="btn btn-default" title="" value="SIGUIENTE" />
                    </div>
                    </form>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <p style="font-weight: bold;font-size: 18px;text-align: justify;">Retiro en bodegas<br>
                El horario de atención en Bodega, es de lunes a viernes de 9:00 a 20:00 horas, los días sábados de 10:00 a 15:00 horas entre el 01 de Diciembre de <?=date("Y")?> al 23 de Diciembre de <?=date("Y")?>. No hay atención los días domingos.</p>
            </div>
            <div class="clearfix"></div>
            </div>
        <br>
	  <br>
	    </div>
	</div>