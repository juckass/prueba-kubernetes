
	<div class="contrainer-fluid nopad wrapper section"  style="margin-top: 5%;height:0;"> 
	    <div class="container">
	    		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 section-title">
    				<h1>¿Cómo quieres recibir tu caja?</h1>
	    		</div>
                <div class="clearfix"></div>

                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 wow fadeIn" data-wow-delay="0.5s">
                    <div class="item-box">
                        <img src="img/img-despacho-domicilio.jpg" class="img-responsive" />
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 wrap-btn">
                            
                            <a class="btn btn-default" title="" href="index.php?op=entrega-domicilio">Despacho a domicilio</a>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 wow fadeIn" data-wow-delay="1s">
                    <div class="item-box">
                        <img src="img/img-retiro-bodega.jpg" class="img-responsive" />
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 wrap-btn">
                            
                            <a class="btn btn-default" title="" href="index.php?op=retiro-bodega">Retiro en bodega</a>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 wow fadeIn" data-wow-delay="1.5s">
                    <div class="item-box">
                        <img src="img/img-retiro-sucursal.jpg" class="img-responsive" />
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 wrap-btn">
                            
                            <a class="btn btn-default" title="" href="index.php?op=entrega-sucursal">Entrega en sucursal</a>
                        </div>
                    </div>
                </div>

	    </div>
	</div>

