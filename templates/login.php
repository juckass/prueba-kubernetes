<!-- Validador RUT -->
<script src="js/jquery.rut.chileno.min.js"></script>

<div class="contrainer-fluid nopad wrapper fadeIn" data-wow-delay="0.5s" style="margin-top: 5%;height:0;">

	<?php
	$error = 0;
	$mensajeError = "";

	if (isset($_REQUEST["error"])) {
		$error = $_REQUEST["error"];
	}

	if ($error == 1) {
		$mensajeError = "No se encuentra Login y Password correcto. Intentelo nuevamente.";
	} elseif ($error == 2) {
		$mensajeError = "Sistema ya no esta operativo.";
	} elseif ($error == 99) {
		$mensajeError = "El RUT ingresado NO es válido.";
	}else if( $error == 3) {
		$mensajeError = "Estimado usuario, el periodo de compras ha concluido.";
	}
	?>

	<script>
	function validar_rut()
	{
		valor_rut = $("#rut").val();
		valor_rut = $.rut.formatear(valor_rut);    
		
		if (valor_rut != "" && valor_rut != "1-9" && valor_rut != "19") {
			validacion = $.rut.validar(valor_rut);
			if (!validacion) {
				alert('El rut: ' + $("#rut").val() + ' es incorrecto');
				$("#rut").val("");
			} else {
				$("#rut").val(valor_rut);
			}
		}
		/*
		$(function() {

				$("form#validacion-live input#rut")
				.rut({
					fn_error : function(input){
						if (input.val() != '1-9' && input.val() != '')
						{
							alert('El rut: ' + input.val() + ' es incorrecto');
							input.val("");
						}
						},
						placeholder: false
					});
			
		});
		*/
	}
	</script>
	<div class="container">
	
		<div class="col-md-7 hidden-xs hidden-sm" style="height: 266px;">
		</div>
		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 pull-right wow fadeInDown" data-wow-delay="1s">
			<div class="panel panel-default panel-pricipal">
				<div class="panel-heading panel-titulo">
					<h2 class="text-center">Ingresa tus datos de usuario</h2>
				</div>
			<div class="panel-body">
				<div class="form-group login">
						<form id="validacion-live" method="post" action="index.php" autocomplete="off">
							<input type="hidden" name="op" id="login" value="login">
							<input type="hidden" name="csrf" id="csrf" value="<?php echo $token; ?>" style="color:black;">
							<input type="text" class="form-control" name="rut" id="rut" placeholder="RUT" onchange="validar_rut();"  style="border-radius: 11px !important;">
							<input type="password" class="form-control" name="password" id="password" placeholder="Contraseña" autocomplete="off" style="border-radius: 11px !important;" >
							<div>
								<center>
									<input type="checkbox" name="modificar_despacho" value="1"><span style="color: black;">Modificar Direcci&oacute;n Despacho</span>
									<br>
								</center>
							</div>
							<input type="submit" value="Ingresar" class="btn btn-default" style="border-radius: 11px !important;">
							<div class="recover-pass">
								<span class="text-password">¿Olvidó su clave? <a href="index.php?op=recoverpw">Recupérela aquí</a></span>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-7 col-lg-7 welcome wow fadeIn login borde-rojo" >
			<?php
			if ($mensajeError) {
				echo "<div class='alert alert-warning' role='alert'><h2>{$mensajeError}</h2></div>";
			}
			?>
			<!-- selecci&oacute;n, -->
			<h2 style="color: black;font-weight: normal;">

			Bienvenidos!<br>

			En esta plataforma, encontrar&aacute;n los diferentes tipos de cajas con el detallle de cada una para su selecci&oacute;n,
			siendo nuestro principal objetivo el generar y proporcionar una excelente experiencia en el proceso..
			</h2>
	
		</div> 
	</div>
</div>
