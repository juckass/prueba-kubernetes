<form method="post" action="index.php">
<input type="hidden" name="op" value="finalizar" />
	<div class="contrainer-fluid nopad wrapper section">
		<div class="container">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 section-title">
				<h1><?php print $titulo; ?></h1>
			</div>
			<div class="clearfix"></div>
			<div class="wrap-white">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 nopad">
					<?php
					if ($caja_seleccionada == 1)
					{
						?>
						<img src="img/caja-clasica.jpg" class="img-responsive" />
						<?php 
					}else if ($caja_seleccionada == 2){
						?>
						<img src="img/caja-cocktail.jpg" class="img-responsive" />
						<?php
					}else if ($caja_seleccionada == 3){
						?>
						<img src="img/caja-sin-alcohol.jpg" class="img-responsive" />
						<?php 
					}
						?>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 checkout bordes" style="border: solid 3px #e63d3a;">
					<?php
					if ($caja_seleccionada_mensaje != "")
					{
						?>
							<label>Tu selección:</label>
							<h2><?php print $caja_seleccionada_mensaje; ?></h2>

							<?php if (isset($numero_pedido) && ($numero_pedido != "")) {?><p class="text-red">NUMERO DE PEDIDO: <?php print $numero_pedido;?></p> <?php } ?>
							<?php if (isset($estado_pedido) && ($estado_pedido != "")) {?><p class="text-red">ESTADO DE PEDIDO: <?php print $estado_pedido;?></p> <?php } ?>
							<p class="text-red">TIPO DE DESPACHO: <?php print $tipo_mensaje;?></p>
							<p><?php print $mensaje_direccion; ?></p>
							<p><strong><?php print $mensaje_destacado; ?></strong></p>
						<?php
					}else{
						?>
						<h2>Aun no has elegido tu caja</h2> 

						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<a class="btn btn-default" title="" href="index.php?op=elige-caja">Elige Caja</a>
						</div>
						<?php
					}

					if ($op == "resumen")
					{
						?>
						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
							<a class="btn btn-default btn-back" title="" href="index.php?op=elige-despacho">Volver</a>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
							<input type="submit" class="btn btn-default" title="" value="CONFIRMAR" />
						</div>
						<?php
					}else if ($caja_seleccionada != ""){
						?>
						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<a class="btn btn-default" title="" href="index.php?op=detalle-caja">Modificar Despacho</a>
						</div>
						<?php
					}
						?>
				</div>
			</div>
		</div>
	</div>
</form>