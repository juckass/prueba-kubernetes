<link rel="stylesheet" type="text/css" href="css/datatables/datatables.min.css"/> 
<link rel="stylesheet" type="text/css" href="css/datatables/buttons.dataTables.min.css"/> 


<script type="text/javascript" src="js/datatables/datatables.min.js"></script>


<script type="text/javascript" src="js/datatables/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="js/datatables/buttons.flash.min.js"></script>
<script type="text/javascript" src="js/datatables/jszip.min.js"></script>
<script type="text/javascript" src="js/datatables/pdfmake.min.js"></script>
<script type="text/javascript" src="js/datatables/vfs_fonts.js"></script>
<script type="text/javascript" src="js/datatables/buttons.html5.min.js"></script>
<script type="text/javascript" src="js/datatables/buttons.print.min.js"></script>


<div class="col-xs-1 col-sm-1 col-md-3 col-lg-3 wrap-btn">
    <a class="btn btn-default" title="" href="index.php?op=administrador&getData=1&excel=1&tipo=total">EXCEL CON TODOS</a>
</div>

<div class="col-xs-1 col-sm-1 col-md-3 col-lg-3 wrap-btn">
    <a class="btn btn-default" title="" href="index.php?op=administrador&getData=1&excel=1&tipo=no_han_realizado">EXCEL NO HAN REALIZADO ELECCION</a>
</div>

<div class="col-xs-1 col-sm-1 col-md-3 col-lg-3 wrap-btn">
    <a class="btn btn-default" title="" href="index.php?op=administrador&getData=1&excel=1&tipo=si_han_realizado">EXCEL SI HAN REALIZADO ELECCION</a>
</div>


<?php 

/*
<table id="tabla_datos"  cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Rut</th>
                <th>Apellido Paterno</th>
                <th>Apellido Materno</th>
                <th>Nombre</th>
                <th>Empresa</th>
                <th>Gls_Empresa</th>
                <th>Gls_Unidad</th>
                <th>Oficina</th>
                <th>Gls_Oficina</th>
                <th>Caja Seleccionada</th>
                <th>Tipo Despacho</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Rut</th>
                <th>Apellido Paterno</th>
                <th>Apellido Materno</th>
                <th>Nombre</th>
                <th>Empresa</th>
                <th>Gls_Empresa</th>
                <th>Gls_Unidad</th>
                <th>Oficina</th>
                <th>Gls_Oficina</th>
                <th>Caja Seleccionada</th>
                <th>Tipo Despacho</th>
            </tr>        
        </tfoot>
        <tbody>
*/        
?>      
<?php 
    /*
     * ANTIGUO : PARTE DE LA SECCION PARA QUE NO SEA SERVER - SIDE
    foreach ($tablaDatos as $res)
    {
        print "
            <tr>
                <td>$res[pri01_rut]-$res[pri01_dv]
                <td>$res[pri01_apePaterno]
                <td>$res[pri01_apeMaterno]
                <td>$res[pri01_nombre]
                <td>$res[pri02_idEmpresa]
                <td>$res[pri02_empresa]
                <td>$res[pri01_unidad]
                <td>$res[pri03_idOficina]
                <td>$res[pri03_oficina]
                <td>$res[pri01_cajaSeleccionada]
                <td>$res[pri01_tipoDespacho]
                <td>$res[pri01_direccionDespacho]
            </tr>
        ";
    }
    */
?>
<?php

/*
    </tbody>
</table>
 
	
<script>
	$(document).ready(function() {
	    $('#tabla_datos').DataTable({
	    	//deferRender:    true,
	        //scrollY:        480,
	        //scrollCollapse: true,
	        //scroller:       true,
	    	//dom: 'Bfrtip',
	    	//buttons: [
	    	//	        'excel', 
	        //    ],        
	        //buttons: [
	        //    'copy', 'csv', 'excel', 'pdf', 'print'
	        //],	
	    	"searching": false,
	    	"iDisplayLength": 10,
	        //"aLengthMenu": [[10, 15, 25, 35, 50, 100, -1], [10, 15, 25, 35, 50, 100, "All"]], 
	    	"aLengthMenu": [[10, 15, 25, 35, 50, 100], [10, 15, 25, 35, 50, 100]],    	
	    	language:{
		    	url: 'js/datatables/es_es.json'
	    	    ,decimal: ","
			  },
			    "processing": true,
		        "serverSide": true,
		        "ajax": "index.php?op=administrador&getData=1"		  
	    });
	} );


</script>
*/
?>
