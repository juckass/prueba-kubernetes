<div class="contrainer-fluid nopad wrapper fadeIn" data-wow-delay="0.5s"  style="margin-top: 5%;height:0;">

	<?php 

		$error = 0;
		if (isset($_REQUEST["error"]))
		{
			$error = $_REQUEST["error"];
		}

		if ($error == 1)
		{
			print "Los password no coinciden. Intentelo nuevamente.";
		}

		$rut_url = "";

		if(isset($_REQUEST['sol']))
		{
			$rut_url = decrypt($_REQUEST['sol']);
		}
		if(isset($_REQUEST['mensaje']))
		{
			?>
			<script type="text/javascript">
				$(document).ready(function(){
					$('#contrasena_cambiada').show();
				});
			</script>
			<?php

		}
	?>
	<script type="text/javascript">
		$(document).ready(function(){
			$('.comprobante').hide();
			
			$('#enviar').on('click', function(){
				console.log("click");
				url = "index.php";
				$.ajax({
					url: url,
					type: 'POST',
					dataType: "json",
					data: { 'op':'mail', 'mail': '<?=((trim($datosUsuario['pri01_despacho_email']) != "")?$datosUsuario['pri01_despacho_email']:$datosUsuario['pri01_email'])?>', 'asunto': 'Comprobante de pedido', 'cuerpo': $('.comprobante').html() },
					success: function(data){
						alert(data.msj);
						window.location = "index.php";
					},
					error: function(){
						alert("Hubo un error al enviar la información");
					}
				});
			});

		});
		function showContent() {
			if ($('#ver').is(':checked')) {
				$('.comprobante').show();
			}else {
				$('.comprobante').hide();
			}
		}
	</script>
	<div class="container" style="width: 90%; text-align: center;" id="cambio_contrasena">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pull-left wow fadeInDown " style="" data-wow-delay="1s" >
			<div class="form-group login borde-rojo">
				<h2 class="text-center">Reenvío de Comprobante</h2>
					<input type="hidden" name="op" id="op" value="changing_pw">
					<input type="hidden" name="setpw" id="setpw" value="1">
					<input type="hidden" name="rut_pw" id="rut_pw" value="<?php print $rut_url;?>">

					<label style="color:black;font-weight:100; padding-bottom: 10px;">
						Estimado <?php echo $datosUsuario['pri01_nombre']; ?> <?php echo $datosUsuario['pri01_apePaterno']; ?> puede observar el comprobante por pantalla y/o recibirlo por correo.

					<div class="form-check">
						<input class="form-check-input" type="checkbox" name="ver" id="ver" onchange="javascript:showContent()" >
						<label class="form-check-label" for="ver">
							Ver en pantalla
						</label>
					</div>

					<div class="comprobante">
						<p style="text-align: left;margin-left: 10%;" >
							Estimado <?php echo $datosUsuario['pri01_nombre']; ?> <?php echo $datosUsuario['pri01_apePaterno']; ?>, usted tiene un (1) pedido con el siguiente detalle :
						</p>
						<br>
						<p style="text-align: left; margin-left: 20%;">
						Nro Pedido : <?php echo $datosUsuario['pri04_id_pedido']; ?><br>
						Caja : <?php echo $config['cajaSeleccionada'][$datosUsuario['pri01_cajaSeleccionada']]; ?><br>
						Tipo de despacho : <?php echo $datosUsuario['pri01_tipoDespacho']; ?><br>
						<?php if($datosUsuario['pri01_tipoDespacho'] == 'domicilio'){ ?>
							Dirección : <?php echo $datosUsuario['pri01_despacho_direccion']; ?><br>
							Nro : <?php echo $datosUsuario['pri01_despacho_numero']; ?><br>
							Comuna : <?php echo utf8_encode($datosUsuario['comuna_nombre']); ?><br>
							Teléfono : <?php echo $datosUsuario['pri01_despacho_telefono']; ?><br>
							Fecha entrega : <?php echo $datosUsuario['pri01_depacho_fecha']; ?><br>
							Comentario : <?php echo $datosUsuario['pri01_despacho_comentarios']; ?></p><br>
						<?php }elseif ($datosUsuario['pri01_tipoDespacho'] == 'sucursal'){ ?>
							Oficina : <?php echo $datosUsuario['oficinaDespacho']; ?><br>
							Región : <?php echo $datosUsuario['region_nombre']; ?><br>
							Dirección : <?php echo $datosUsuario['pri03_direccion']; ?><br>
							Fecha entrega : <?php echo $datosUsuario['pri01_retiro_fecha']; ?><br>
						<?php }elseif ($datosUsuario['pri01_tipoDespacho'] == 'bodega'){ ?>
							Dirección : Las Américas 777, Cerrillos. Santiago Región Metropolitana.<br>
							Fecha entrega : <?php echo $datosUsuario['pri01_retiro_fecha']; ?><br>
						<?php } ?>

						<p style="text-align: left;margin-left: 10%;" >
							Saludos.
						</p>
					</div>

					<div class="form-group">
						<button id="enviar" class="btn btn-default">Enviar</button>
					</div>
			</div>
		</div>
	</div>
</div>