<?php
    $nombre = "";
    if (isset($_SESSION['nombre']))
    {
        $nombre = $_SESSION['nombre'] ;
    }
    
    $empresa = "";
    if (isset($_SESSION['empresa']))
    {
        $empresa = $_SESSION['empresa'] ;
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <!--<link rel="icon" href="../../favicon.ico">-->

        <title>CAJAS DE NAVIDAD GRUPO SANTANDER <?=date("Y")?></title>

        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">


        <!-- Custom styles for this template -->
        <link href="css/custom.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">



        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- WOW.js -->
        <script src="js/wow.min.js"></script>
        <link href="css/animate.css" rel="stylesheet">
        <script>
            new WOW().init();
        </script>

        <!-- Bootstrap & jQuery core  -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"><\/script>')</script>
        <script src="js/bootstrap.min.js"></script>
    </head>

<body class="wow fadeIn">
<div class="container-fluid nopad header">
    <div class="container">
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 nopad ">
            <a              <?php 
                    if ( (!isset($_SESSION['tipo_administrador'])) || ($_SESSION['tipo_administrador'] == "0") )
                    {
                        if ( (!isset($_SESSION['cajaSeleccionada'])) || $_SESSION['cajaSeleccionada'] == "" ) 
                        {
                            ?>
                href="index.php?op=elige-caja"
                            <?php
                        } 
                    }
                            ?>>
                <img src="img/logo-santander-new.jpg" class="img-responsive" />
            </a>
        </div>
        <div class="col-lg-6 text-center visible-md visible-lg title">PROCESO DE SELECCIÓN DE CAJAS DE NAVIDAD GRUPO SANTANDER <?=date("Y")?></div>

        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 nopad prilogic pull-right text-right">
            <a              <?php 
                    if ( (!isset($_SESSION['tipo_administrador'])) || ($_SESSION['tipo_administrador'] == "0") )
                    {
                        if ( (!isset($_SESSION['cajaSeleccionada'])) || $_SESSION['cajaSeleccionada'] == "" )
                        {
                            ?>
                href="index.php?op=elige-caja"
                            <?php 
                        }
                    }
                            ?>>
                <img  class="img-responsive pull-right" src="img/logo-prilogic.png" />
            </a>
        </div>                          
    </div>
</div>
<?php
        if ($nombre != "")
        {?>
            <div class="container nopad user-data">
                <div class="col-xs-12 col-sm-12 col-md-12 cl-lg-12">
                    <p><b><?php print htmlentities($nombre);?>, <?php print htmlentities($empresa);?></b> | 
                        <?php 
                        if ( (!isset($_SESSION['tipo_administrador'])) || ($_SESSION['tipo_administrador'] == "0") )
                        {
                            $ops = array("changepw", "reenvio", "cierre");
                            if (!in_array($_REQUEST['op'], $ops))
                            {   ?>
                        <a href="index.php?op=cuenta">Mi cuenta</a> | 
                                <?php 
                            }
                        }
                                ?>
                        <a href="index.php?op=logout">Salir</a>
                    </p>
                </div>
            </div>
            <?php
        }
        ?>