
	<div class="contrainer-fluid nopad wrapper section">
	    <div class="container">
	    		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 section-title">
    				<h1>Elige tu Caja</h1>
	    		</div>
                <div class="clearfix"></div>

                <div class="box-detail">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 nopad">
<?php 
    $v = 0;
    if (isset($_SESSION['caja_seleccionada']) && $_SESSION['caja_seleccionada'] != "" && $_SESSION['caja_seleccionada'] > 0 && $_SESSION['caja_seleccionada'] < 4)
    {
        $v = $_SESSION['caja_seleccionada'];
    }
    else if (isset($_REQUEST['v']) && $_REQUEST['v'] != "" && $_REQUEST['v'] > 0 && $_REQUEST['v'] < 4)
    {
        $v = strip_tags(htmlentities($_REQUEST['v']));
        $_SESSION['caja_seleccionada'] = $v;
    }
    else
    {
        header("Location: index.php?op=elige-caja");
        exit;
    }

    if ($v == 1)
    {
?>                        
        <img src="img/caja-clasica.jpg" class="img-responsive" />
<?php 
    }
    else if ($v == 2)
    {
?>                        
        <img src="img/caja-cocktail.jpg" class="img-responsive" />
<?php 
    }
    else if ($v == 3)
    {
?>                        
        <img src="img/caja-sin-alcohol.jpg" class="img-responsive" />
<?php 
    }
?>

                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <h2>
<?php 

   
    $texto = "";
    if ($v == 1)
    {
        print "Clásica";
        $texto = "
<li>1   Nescafe Fina Selección Frasco 170 Grs</li>
<li>1   Piña Rodajas Esmeralda 822 Grs</li>
<li>1   Duraznos  Dos Caballos 820 Grs</li>
<li>1   Papa Frita Kryzpo Queso  140 Gr</li>
<li>1   Gran Surtido Galletas Costa 335 Grs</li>
<li>1   Atun Lomito Aceite Angelmo 170gr</li>
<li>1   Palmitos Enteros Belbi 800 Gr</li>
<li>1   Macha Al Natural Angelmo 190 Gr</li>
<li>1   Caramelo Toffe Ambrosoli 450 Gr</li>
<li>1   Vino Res Cab/sauvinong Misiones De R 750 Cc</li>
<li>1   Ron Havana Club 750 Cc Añejado </li>
<li>1   Choritos aceite 190 Gr San José</li>
<li>2   Sahne Nuss Almendras Bañadas 60 Gr</li>

<li>1   Mayonesa Hellmans Supreme Light 444 Gr</li>
<li>2   Galleta Crackelet Costa 85 Grs</li>
<li>1   Chocolate Lata Corazon Rojo 102 Gr Hershey</li>
<li>1   Pan De Pascua Castaño Navideño 800 Grs</li>
<li>1   Mix Mani, Pasas, Almendras Lata Marco Polo 142 Grs</li>
<li>1   Espumante Valdivieso Brut 750 Cc</li>
<li>1   Choclitos Cocktail Esmeralda Tarro De 230 Grs</li>                    
                  
        ";
    }
    elseif ($v == 2)
    {
        print "Cocktail";
        $texto = "
<li>1   Pina Rodajas Esmeralda 822 Gr</li>
<li>1   Duraznos  Dos Caballos 820 Grs.</li>
<li>1   Papa Frita Kryzpo Queso  140 Gr</li>
<li>1   Atun Lomito Aceite Angelmo 170gr</li>
<li>1   Palmitos Enteros Belbi 800 Gr</li>
<li>1   Macha Al Natural Angelmo 190 Gr</li>
<li>1   Choclito Coctail Esmeralda 230 Gr</li>
<li>1   Vino Res Cab/sauvinong Misiones De R 750 Cc</li>
<li>2   Aceitunas Rellenas  Excelencia Pimiento Asado 300 Gr</li>
<li>1   Mayonesa Hellmans Supreme Light 444 Gr</li>
<li>1   Choritos aceite 190 Gr San José</li>
<li>2   Sahne Nuss Almendras Bañadas 60 Gr</li>
<li>1   Tortilla Maiz Pancho Villa 320 Gr</li>
<li>1   Salsa Queso Cheddar Pancho Villa 200 Gr</li>
<li>1   Salsa Chunky Pancho Villa 200 Gr</li>
<li>2   Galleta Crackelet Costa 85 Grs</li>
<li>1   Pan De Pascua Castaño Navideño 800 Grs</li>
<li>1   Mix Mani, Pasas, Almendras Lata Marco Polo 142 Grs</li>
<li>1   Vodka Absolut T  750 Cc Tradicional Sin Sabor</li>
<li>1   Espumante Valdivieso  Brut 750 Cc</li>
<li>1   Esparragos Esmeralda Frasco De 230 Grs.</li>    
        ";
    }
    elseif ($v == 3)
    {
        print "Sin Alcohol";
        $texto = "
<li>1   Nescafe Fina Selección Frasco 170 Grs.</li>
<li>1   Piña Rodajas Esmeralda Tarro 822 Grs.</li>
<li>1   Duraznos  Dos Caballos Tarro 820 Grs.</li>
<li>1   Papa Frita Kryzpo Queso  140 Grs.</li>
<li>1   Gran Surtido Galletas Costa Caja 335 Grs</li>
<li>1   Atun Lomito Aceite Angelmo 170 Grs</li>
<li>1   Palmitos Enteros Belbi 800 Grs</li>
<li>1   Macha Al Natural Angelmo 190 Grs</li>
<li>1   Caramelo Toffe Ambrosoli Bolsa 450 Grs</li>
<li>1   Pepinillo dulce, frasco 430 Grs, rodajas (Bimaria)</li>
<li>1   Aceitunas rellenas C/pasta de pimiento 240 Grs.</li>
<li>2   Sahne Nuss almendras bañadas 60 Grs</li>
<li>1   Choritos aceite 190 Gr San José</li>
<li>1   Mayonesa Hellmans Supreme Light 444 Grs</li>
<li>2   Galleta Crackelet Costa Paquete 85 Grs</li>
<li>1   Chocolate Lata Corazon Rojo 102 Gr Hershey</li>
<li>1   Pan De Pascua Castaño Navideño 800 Grs</li>
<li>1   Mix Mani, Pasas, Almendras Lata Marco Polo Tarro 142 Grs.</li>
<li>1   Fondos de alcachofas canales frasco de 1 Kg</li>
<li>1   Choclitos Cocktail Esmeralda Tarro De 230 Grs</li>
<li>1   Esparragos Esmeralda Frasco De 230 Grs.</li>
<li>1	Papayas al Jugo Guallarauco Frasco de 340 Grs.</li>
        ";
    }
    
?>
                        </h2>
                        <ul>
<?php 
    print $texto;
?>
                        </ul>

                        <form id="elige-despacho" method="post" action="index.php">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
<label for="lugar_despacho">Elige despacho:</label>
<select name="lugar_despacho" id="lugar_despacho" class="form-control">
    <?php 
    if($_SESSION['lugar_despacho'] == "rm"){?>
    <option value="rm" <?php if (isset($_SESSION['lugar_despacho']) && ($_SESSION['lugar_despacho'] == "rm")) {print " selected='selected' ";} ?> >Despacho Región Metropolitana</option>
    <?php } else if ($_SESSION['lugar_despacho'] == "otras_regiones"){ 
    ?>
    <option value="otras_regiones" <?php if (isset($_SESSION['lugar_despacho']) && ($_SESSION['lugar_despacho'] == "otras_regiones")) {print " selected='selected' ";} ?>>Despacho Otras Regiones</option>
    <?php } ?>
</select>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
<a class="btn btn-default btn-back" title="" href="index.php?op=elige-caja">Volver</a>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
<input type="submit" value="Elige esta caja" class="btn btn-default">
<input type="hidden" name="op" value="elige-despacho">
                        </div>
                        </form>
    
                    </div>
                </div>

	    </div>
	</div>
	
	
