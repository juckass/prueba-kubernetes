
    <!-- datePicker -->
    <script src="js/jquery-ui.min.js"></script>
    <link href="css/jquery-ui.min.css" rel="stylesheet">
    <script src="js/jquery.mask.min.js"></script>

  

    <form method="POST" action="index.php" >
    <input type="hidden" name="op" value="resumen" />
    <input type="hidden" name="tipo" value="domicilio" />
    
     
	<div class="contrainer-fluid nopad wrapper section">
	    <div class="container">
                <div class="box-detail">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 nopad">
                        <?php 
                            $v = 0;
                            if (isset($_SESSION['caja_seleccionada']) && $_SESSION['caja_seleccionada'] != "" && $_SESSION['caja_seleccionada'] > 0 && $_SESSION['caja_seleccionada'] < 4)
                            {
                                $v = $_SESSION['caja_seleccionada'];
                            }

                            if ($v == 1)
                            {
                        ?>                        
                                <img src="img/caja-clasica.jpg" class="img-responsive" />
                        <?php 
                            }
                            else if ($v == 2)
                            {
                        ?>                        
                                <img src="img/caja-cocktail.jpg" class="img-responsive" />
                        <?php 
                            }
                            else if ($v == 3)
                            {
                        ?>                        
                                <img src="img/caja-sin-alcohol.jpg" class="img-responsive" />
                        <?php 
                            }
                        ?>
                    </div>
                   
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 bordes" style="border: solid 3px #e63d3a;" >
                        <p class="text-red form-title">Completa los datos de despacho</p>

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <input class="form-control" name="direccion" type="text" value="<?php echo $res['pri01_despacho_direccion']?>" placeholder="DIRECCION" maxlength="255"  required="required" />
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <input class="form-control" name="numero" type="text"  value="<?php echo $res['pri01_despacho_numero']?>" placeholder="NUMERO" maxlength="24" required="required" />
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <select id="comuna" name="comuna" class="form-control-option" required="required">
                                <option  value="">Selecciona Comuna</option>
                                <?php 
                                global $listaComunas;
			
                                foreach ($listaComunas as $key => $valor)
                                {
                                    if($res['comuna_id'] == $key ){
                                    echo  "<option value='$key|$valor' selected='selected' >$valor</option>\n";
                                    }else{
                                    echo  "<option value='$key|$valor' >$valor</option>\n";
                                    }
                                
                                }

                                ?>                                
                            </select>
                        </div>
                        
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <input class="form-control soloNumeros" id="telefono" name="telefono" type="text" placeholder="N° DE CELULAR" value="<?php echo $res['pri01_despacho_telefono']?>"  maxlength="31" required="required"/>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <input class="form-control" name="email" type="text" placeholder="EMAIL"  maxlength="63"  value="<?php echo $res['pri01_despacho_email']?>" required="required"/>
                        </div>

                        <!--<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <input class="form-control" name="fecha_despacho" type="text" id="datepicker" placeholder="FECHA DE DESPACHO"  maxlength="24" required="required" readonly="readonly" />
                        </div>-->

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <textarea class="form-control" name="comentarios" placeholder="COMENTARIOS" maxlength="999" ><?php echo $res['pri01_despacho_comentarios']?></textarea>
                        </div>
                        
                        
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <a class="btn btn-default btn-back" title="" href="index.php?op=elige-despacho" >VOLVER</a>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <input type="submit" class="btn btn-default" title="" value="SIGUIENTE" />
                        </div>
                        
                    </div>
                  
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 section-title">
                        <p style="font-weight: bold;font-size: 18px;text-align: justify;">
                            Despacho a domicilio - Región Metropolitana
                            <br>
                            Se realizará de lunes a viernes,<br>
                            desde las 10 horas hasta las 19 horas,<br>
                            los días sábados y domingos,<br>
                            la entrega se realizará entre las 10 horas y las 18 horas.
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </div>
	    </div>
	</div>
	</form>

    <script type="text/javascript">

//var unavailableDates = ["19-10-2017","20-10-2017","11-11-2017"]; 
var unavailableDates = [<?php print $fechas_no_disponibles; ?>];

function unavailable(date) {
  dmy = date.getDate() + "-" + (date.getMonth()+1) + "-" + date.getFullYear();
  if ($.inArray(dmy, unavailableDates) < 0) {
    return [true,"",""];
  } else {
    return [false,"",""];
  }
}   

function mascara(){
    $codigo = "(+569)";
    $valor = $('#telefono').val();
    
    var n = $valor.search(/([+569])/);
    console.log(n);
    if(n == 0  || n == -1 ){
        $('#telefono').val("(+569) " + $('#telefono').val());
    }
}

jQuery(document).ready(function(){
    mascara();
    $('#telefono').keypress(function() {
        mascara();
    });

    $('#telefono').blur(function() {
        mascara();
    });

    $('.soloNumeros').on('keypress', function (e) {
        if (!/\d+/.test(e.key)) {
            return false;
        }

        return;
    });
    
    $('.soloNumeros').blur(function (e) {
        if (!/\d+/.test(e.key)) {
            return false;
        }

        return;
    });
  $("#telefono").on('paste', function(e){
    e.preventDefault();
    //alert('Esta acción está prohibida');
  });
    
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
        };

              $.datepicker.setDefaults($.datepicker.regional['es']);
                          
        jQuery( "#datepicker" ).datepicker(
                {
                    dateFormat: "dd-mm-yy",
                    minDate: new Date(2017, 11 - 1, 13),
                    maxDate: new Date(2017, 12 - 1, 22),
                    beforeShowDay: unavailable 
                });
    });
    
</script>

