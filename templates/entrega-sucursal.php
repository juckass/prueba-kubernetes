<?php 

global $region;

/*$filtro_region = "";
if (isset($_SESSION['lugar_despacho']))
{
    $filtro_region = $_SESSION['lugar_despacho'];
}*/

//"rm"
//"otras_regiones"
?>
<div class="contrainer-fluid nopad wrapper section">
	<div class="container">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 section-title ">
			<h1>Entrega en sucursal</h1>
			<!--<p>Región seleccionada</p>-->
		</div>
		<div class="clearfix"></div>

		<div class="box-detail ">
			<!--<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 column-search">
			    <p class="text-red form-title">BUSQUEDA DE SUCURSALES</p>

			    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			    <form>
			        <select name="region" id="region" class="form-control" onchange='cambiarResultado();'>
						
			          <?php  if ($filtro_region != "rm") { ?>  
			            <option selected='selected' value="0">REGION</option>
			            <?php } ?>
			            <?php 
			                
			                			    
			                foreach ($listaRegiones as $key => $valor)
			                {
			                    if ( 
			                        ( ($filtro_region == "rm") && ($key == "13") )
			                        || ( ($filtro_region == "otras_regiones") && ($key != "13") )
			                    )
			                    {
			                        print "<option value='$key' ";
			                        if ($filtro_region == 'rm') { print " selected='selected' "; }  
			                        print ">" . $valor->ordinal . " - " . htmlentities($valor->nombre) ."</option>\n";
			                    }
			                }
			            ?>
			        </select>
			    </form>
			    </div>
			</div>-->

			<div id="resultado" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 column-search ">

			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	function cambiarComuna(input)
	{
		var selectedValue = input.value;
		$.ajax({
			url: 'index.php?op=entrega-sucursal-comuna&region_id=' + selectedValue,
			type: 'GET',
			success: function( returnedData ) {
				$( '#comuna' ).html( returnedData );
			}
		});
	}

	function cambiarResultado()
	{
		var selectedValue = "<?php echo $region['numero']; ?>";
		//alert(selectedValue);
		$.ajax({
			url: 'index.php?op=entrega-sucursal-resultado&region_id=' + selectedValue,
			type: 'GET',
			success: function( returnedData ) {
				$( '#resultado' ).html( returnedData );
				$( '#sucursalName' ).text ('<?php echo $region['nombre']; ?>');
			}
		});
	}

	$(document).ready(function(){
		cambiarResultado();
	});
</script>