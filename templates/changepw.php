

	<div class="contrainer-fluid nopad wrapper fadeIn" data-wow-delay="0.5s">

<?php 

$error = 0;
if (isset($_REQUEST["error"]))
{
    $error = $_REQUEST["error"];
}

if ($error == 1)
{
    print "Los password no coinciden. Intentelo nuevamente.";
}

$rut_url = "";

if(isset($_REQUEST['sol']))
{
    $rut_url = decrypt($_REQUEST['sol']);
}
if(isset($_REQUEST['mensaje']))
{
   ?>
        <script type="text/javascript">
            $(document).ready(function(){
                $('#contrasena_cambiada').show();
            });
        </script>
    <?php

}
else
{
    ?>
        <script type="text/javascript">
            $(document).ready(function(){
                //$('#mensaje_guardado').modal('show');
                $('#cambio_contrasena').show();
            });
        </script>
    <?php
}
?>
    <script>
        function valida_form()
        {
            $('#password1').val();
            $('#password2').val();
            if( $('#password1').val() !== $('#password2').val() )
            {
                alert("Los claves no coinciden");
                $('#password1').val("");
                $('#password2').val("");
                return false;
            }
            return true;
        }
    </script>
	

	    <div class="container" style="display: none;" id="cambio_contrasena">
	    		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 pull-left wow fadeInDown" data-wow-delay="1s" >
	    			<div class="form-group login">
	    				
	    				<h2 class="text-center">Cambio de Password</h2>

	  					<form id="validacion-live" method="post" action="index.php">
                            <input type="hidden" name="op" id="op" value="changing_pw">
                            <input type="hidden" name="setpw" id="setpw" value="1">
                            <input type="hidden" name="rut_pw" id="rut_pw" value="<?php print $rut_url;?>">
                            <label style="color:black;font-weight:100; padding-bottom: 10px;"><strong>Hola,</strong> escribe a continuación tu nueva contraseña:</label>
                            <input type="text" class="form-control" name="rut_disabled" id="rut_disabled" disabled value="<?php print $rut_url;?>">
                            <div class="form-group">
                                <?php
                                if ($error == 7)
                                {
                                    print "<div class='alert alert-danger'>La Contrase&ntilde;a es requerida</div>";
                                }elseif($error == 8){
                                    print "<div class='alert alert-danger'>Formato inv&aacute;lido. M&iacute;nimo 8, M&aacute;ximo 15, Al menos una letra may&uacute;scula, Al menos una letra min&uacute;scula, valores alfan&uacute;mericos</div>";
                                }elseif($error == 9){
                                    print "<div class='alert alert-danger'>Deben coincidar las contraseñas</div>";
                                }
                                ?>
                                <div class="help-block" style="font-size: 10px; color: gray; padding-left: 10px; padding-right: 10px;">M&iacute;nimo 8 - M&aacute;ximo 15, Al menos una letra may&uacute;scula, valores alfan&uacute;mericos</div>
                                <input type="password" class="form-control" name="password1" id="password1" placeholder="Contraseña" required="required" pattern="^[A-Za-z\d_]{8,15}$">
                            </div>
                            <input type="password" class="form-control" name="password2" id="password2" placeholder="Repita Contraseña" required="required">
                            <input type="submit" value="Actualizar Contraseña" class="btn btn-default">
						</form>

					</div>

	    		</div>
	    </div>
        <div class="container" style="display: none;" id="contrasena_cambiada">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 wow fadeInDown" data-wow-delay="1s">
                <div class="form-group login">

                    <h2 class="text-center">El cambio de clave se realizó con éxito</h2>

                    <form method="post" action="index.php">
                        <input type="submit" value="Continuar" class="btn btn-default">
                    </form>
                </div>
            </div>
        </div>
	</div>







	
