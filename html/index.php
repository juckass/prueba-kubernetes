<?php
session_start();

ob_start();

$rutaActual = dirname(__FILE__);
include_once "{$rutaActual}/../config.php";
include_once "{$rutaActual}/../library/helper.php";
include_once "{$rutaActual}/../library/mysql.php";
include_once "{$rutaActual}/../controllers/Utils.php";


$utils = Utilidades::getInstance();
$utils->generarCSRF();
$token = $utils->getCSRF();

if (!isset($_REQUEST['op']) || ($_REQUEST['op'] != htmlentities(strip_tags($_REQUEST['op'])) )) {
    $skip_redirect = 1;
    include_once dirname(__FILE__) ."/../controllers/logoutController.php";
}

include_once dirname(__FILE__) ."/../templates/header.php";
//include_once dirname(__FILE__) ."/../templates/mantenimiento.php";exit;
/*Agregado */
if (($_REQUEST['op'] == "recoverpw")) {
    include_once dirname(__FILE__) ."/../templates/recoverpw.php";
    exit();
}

if (($_REQUEST['op'] == "changepw") && ($_REQUEST['sol'] != '')) {
    include_once dirname(__FILE__) ."/../templates/changepw.php";
    exit();
}

if (!isset($_REQUEST['op']) || ($_REQUEST['op'] != htmlentities(strip_tags($_REQUEST['op'])) )) {
 

    include_once dirname(__FILE__) ."/../templates/login.php";
    footer();
    exit;
} else {
    if (!b_connect()) {
        $mensaje = "Por favor intente más tarde.";
        include_once dirname(__FILE__) ."/../templates/error.php";
        footer();
        exit;
    }

    if (($_REQUEST['op'] == "recoverpword")) {
        include_once dirname(__FILE__) ."/../controllers/recoverpwController.php";
        exit();
    }

    if (($_REQUEST['op'] == "changing_pw")) {
        include_once dirname(__FILE__) ."/../controllers/changepwController.php";
        exit();
    }

    if (!isset($_SESSION['rut']) && ($_REQUEST['op'] == "login")) {
        include_once dirname(__FILE__) ."/../controllers/loginController.php";
    } elseif (!isset($_SESSION['rut'])) {
        include_once dirname(__FILE__) ."/../templates/login.php";
        footer();
        exit;
    }
    
    include_once dirname(__FILE__) ."/../controllers/index_controller.php";
}

footer();
?>
